# Project README file

## How I Implemented my Functionality

My implementation for the echo client warm-ups is borrowed heavily from the available source code on Udacity. This is simple implementation as it assumes that sends
and reads contain all of the content. Therefore, most of the code required is simply boiler-plate code to set up the sockets and appropriate network addresses.
I used Beej's Guide to Network Programming and the online code samples from "TCP/IP Sockets in C" for the Transferfile warm-up. Specifically I took ideas from the
online code sample: http://cs.baylor.edu/~donahoo/practical/CSockets/code/TCPEchoClient.c
The client runs and keeps receiving data from the server until the socket connection no longer returns data. This is in a similar spirit to the Transfer file description.

For part 1, I leveraged the concepts from the transfer file warm-up to make sure that all reads and writes to the socket fully flushed the contents of the data buffer. I
read a lot about string processing in C and string matching in order to check for the end of header marker "\r\n\r\n". Specifically I used the strstr() function to check
for the marker which requires that I manually set the '\0' character at the end of the buffer in order for the function to process it as a string. After I found the
end of header maker I used sscanf() with a format string to find the file request path or the file length. Finally, I had to check for closed connections on the socket
and validate each of the contents according to the rules in the project description. I had many submit iterations with Bonnie tweaking these status and return values to
make sure they returned the right values.

For part 2, the MT Client used concepts from: http://cs.baylor.edu/~donahoo/practical/CSockets/code/TCPEchoServer-Thread.c and I leveraged heavily the concepts from the LLNL
Pthreads tutorial. This is a simple implementation which just creates nthreads with each thread making nrequests to the server.
The MT Server leverages the steque.h library which allows the creation of a thread-safe queue to enqueue requests and process them in a thread-safe way. This effectively creates
a layer of indirection between the boss and the workers to allow for efficient and scalable interaction between them.

## How I Tested My Components

For the warm-ups I was able to debug any small bugs that I had by simply using GDB and setting breakpoints to inspect values. I used a lot of the code which is
available on the Udacity: https://classroom.udacity.com/courses/ud923/lessons/3518179061/concepts/34870090650923
I did not spend that much time debugging or testing the warm-ups because the implementations are mostly boiler plate code. I spent most of the time reading the
documentation and references linked from the project description.

By far the most testing was with Part 1. I spent a lot of time debugging the string parsing code and making sure that the correct header lengths and content
markers were set in my final implementation. The following Piazza post also helped me out as I was working on the GFServer first and I could test it
without a working GFClient implementation: "Testing gfserver without a client" https://piazza.com/class/jc6jlz153n3462?cid=142
Additionally, I had to test and make many iterations to make the correct return values and statuses were set in the case that the socket was disconnected in
the middle of the transmission. In my opinion, I wish the project description was more explicit in what was expected as I had to try several statuses by
submitting to finally get the expected return value and status.

For Part 2 I got the implementations working for 1 thread for both GFClient GFServer and tested to make sure everything worked as expected and then
scaled nthreads to bigger numbers.
This made debugging significantly easier as there was only one client request or server worker running and I could focus on the code logical flow without
worrying about contention for shared resources. After knowing that I had setup my concurrency data structures properly I could confidently scale up the number
of threads and know that it would still work properly.

## Challenges Overcame

This project had a lot of small challenges. Firstly, I had never done network socket programming or Pthreads in C before. I have extensive experience
with thread programming in Java and Go so the thread aspect of this assignment was relatively easy for me. Also all of the concepts for building a
thread-safe request queue were discussed in the Udacity lecture videos.
After spending about two days reading through Beej's Guide to Network Programming I was easily able to pass the warm-up exercises.
The warm-ups followed very easily from the provided references in the project description.
Note that I found a bug in the link for the book "TCP/IP Sockets in C" as described in my post
here: https://piazza.com/class/jc6jlz153n3462?cid=372

The GfLib (part 1) took a lot of time to complete because it required a lot of attention to detail to make sure all of the return values and statuses
were set properly. I also had a really bad bug where I was sending a "\0" after the "\r\n\r\n" end of header marker. This was causing me to fail tests
for both the client and server which was really tricky to track down and debug. A key challenge of this project is having a reasonable well working implementation
of the gfserver first and then working on the gfclient while still continuously working on iterations for the gfserver.

I was very rusty in my basic C skills: string processing, pointer arithmetic for buffers, and memory management so this made simple tasks in Part 1 of the project
very time consuming. I had several memory leaks in my early iterations and I was able to solve them by putting variables on the stack to the extent possible. Also,
doing very good record keeping of objects living on the heap and making sure to properly free them in the case of both successful and failed results. I originally
tried to use strtok() for my string process of the request headers but I had significant issues with running in to seg-faults so I opted instead to use sscanf().
Strtok() can be very confusing to use as it modifies the input string which can easily cause seg faults at runtime if the input string is const char data type.

The Multithreaded Client was very easy to implement as most of the code was already provided. It just required learning how to spawns threads as was discussed in
the really great tutorial: https://computing.llnl.gov/tutorials/pthreads
The Multithread Server was trickier to implement but fortunately, I have often asked interview question to job candidates to implement a thread-safe linked list
in Java so I was familiar with a similar implementation.
I avoided a spin-lock or a busy-waiting approach for the worker threads by correctly applying a wait/broadcast approach. I again used the LLNL PThreads tutorial
along with Ada's lecture videos to implement this. I also was already familiar with Java's wait()/notifyAll() methods so implementing this was not very difficult.


## Known Bugs/Issues/Limitations

* Each part of my project passes all of the Bonnie tests so there are no known bugs I have at this moment.
* Given that each server thread is simply reading a file from disk and sending it back to the client, the processing is I/O bounded.
  Therefore a known limitation of this design may be to move to a single threaded responses handler on the server similar to Node.js.
  There is an OS limit to the number of threads which can be created on the server and if you are trying to solve the C10K or C10M problem
  you will need a lot of threads to process all of the concurrent connections. Our current design may be a limitation and moving to a single
  threaded callback architecture may make more sense. See this video by the Node.js creator for more info: https://www.youtube.com/watch?v=ztspvPYybIY

## References

* https://classroom.udacity.com/courses/ud923/lessons/3518179061/concepts/34870090650923
* https://beej.us/guide/bgnet/html/multi/syscalls.html#bind
* https://beej.us/guide/bgnet/html/multi/index.html
* http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html - Tutorial essentially the same as above. Same code examples too
* https://stackoverflow.com/questions/14424378/socket-programming-bind-system-call
* https://computing.llnl.gov/tutorials/pthreads
* https://stackoverflow.com/questions/17096990/why-use-bzero-over-memset
* https://stackoverflow.com/questions/19971858/c-socket-send-recv-vs-write-read
* https://stackoverflow.com/questions/11414191/what-are-the-main-differences-between-fwrite-and-write
* http://en.cppreference.com/w/c/io/fread
* http://pubs.opengroup.org/onlinepubs/009695399/functions/fopen.html
* https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm
* https://stackoverflow.com/questions/29638598/strtok-why-you-have-to-pass-the-null-pointer-in-order-to-get-the-next-token
* http://www.java2s.com/Code/C/String/Truncatestringbydelimiterhowtousestrtok.htm
* https://gist.github.com/efeciftci/9120921
* https://stackoverflow.com/questions/15432123/using-pointer-after-free
* https://stackoverflow.com/questions/32594873/malloc-array-of-characters-dynamic-vs-static-c
* https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm
* https://stackoverflow.com/questions/3053757/read-from-socket
* https://www.tutorialspoint.com/c_standard_library/c_function_memcpy.htm
* https://cs.nyu.edu/courses/spring12/CSCI-GA.3033-014/Assignment1/function_pointers.html
* https://www.codeproject.com/Tips/800474/Function-Pointer-in-C-Struct
* http://www.cplusplus.com/reference/cstdio/snprintf/
* https://www.geeksforgeeks.org/sprintf-in-c/
* https://www.tutorialspoint.com/c_standard_library/c_function_sscanf.htm
* https://stackoverflow.com/questions/13460934/strcpy-using-pointers
* https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
* https://stackoverflow.com/questions/33036546/segment-fault-when-using-strtok
* https://stackoverflow.com/questions/5925405/problem-with-strtok-and-segmentation-fault
* https://www.tutorialspoint.com/c_standard_library/c_function_strcpy.htm
* https://stackoverflow.com/questions/2827714/whats-the-easiest-way-to-parse-a-string-in-c
* http://www.cplusplus.com/reference/cstdio/sscanf/
* https://www.tutorialspoint.com/c_standard_library/c_function_strncmp.htm
* https://www.tutorialspoint.com/c_standard_library/c_function_sscanf.htm
* https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm
* https://stackoverflow.com/questions/9537072/properly-using-sscanf
* http://www.dreamincode.net/forums/topic/203105-reading-from-socket-into-buffer/
* https://www.tutorialspoint.com/c_standard_library/c_function_memcpy.htm
* https://www.codingunit.com/c-reference-stdlib-h-function-free
* https://www.tutorialspoint.com/c_standard_library/c_function_strncmp.htm
* https://stackoverflow.com/questions/14300238/how-to-trim-a-string-of-format-char
* https://www.tutorialspoint.com/c_standard_library/string_h.htm
* http://joequery.me/code/snprintf-c/
* http://pubs.opengroup.org/onlinepubs/009696899/functions/send.html
* https://stackoverflow.com/questions/34116883/can-you-explain-the-method-of-finding-the-offset-of-a-buffer-when-looking-for-bu
* http://forums.devshed.com/programming-42/memcpy-offset-285227.html
* https://stackoverflow.com/questions/2172943/size-of-character-a-in-c-c
* http://forums.devshed.com/programming-42/memcpy-offset-285227.html
* https://www.tutorialspoint.com/c_standard_library/c_function_calloc.htm
* https://stackoverflow.com/questions/15739490/should-use-size-t-or-ssize-t
* https://stackoverflow.com/questions/4204915/please-explain-the-exec-function-and-its-family
* http://cs.baylor.edu/~donahoo/practical/CSockets/textcode.html
* https://www.cs.swarthmore.edu/~newhall/unixhelp/c_codestyle.html
