#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>

#include "gfserver.h"
#include "gfserver-student.h"

#define BUFSIZE 512

/*
 * Modify this file to implement the interface specified in
 * gfserver.h.
 */
struct gfserver_t {
    unsigned short port;
    int max_npending;
    ssize_t (*handler)(gfcontext_t *, char *, void*);
    void* handlerarg;
};

struct gfcontext_t {
    char *path;
    int client_socket_fd;
    gfstatus_t status;
};

void gfs_abort(gfcontext_t *ctx) {
    if (!ctx) {
        return;
    }

    if (ctx->client_socket_fd > 0) {
        close(ctx->client_socket_fd);
    }
}

gfserver_t* gfserver_create() {
    return calloc(1, sizeof(gfserver_t));
}

ssize_t gfs_send(gfcontext_t *ctx, void *data, size_t len) {
    ssize_t num_bytes_remaining = len;
    ssize_t num_bytes_sent;
    ssize_t num_bytes_offset = 0;

    /* WARNNG: You cannot assume you can call free() on data */
    /* Doing a byte offset in the way below is the only approach flexible enough to work */
    /* with both statically (char[]) and dynamically (char *) sized pointers */
    while (num_bytes_offset < len) {
        num_bytes_sent = write(ctx->client_socket_fd, data + num_bytes_offset, num_bytes_remaining);
        if (0 > num_bytes_sent) {
            // Something went wront -- abort
            return -1;
        }
        num_bytes_remaining -= num_bytes_sent;
        num_bytes_offset += num_bytes_sent;
    }

    return num_bytes_offset;
}

ssize_t parse_path_from_header(gfcontext_t *ctx, char *buffer) {
    // The scheme is always GETFILE
    // The method is always GET
    const char *format = "GETFILE GET %s";
    int matched_valid_request = sscanf(buffer, format, ctx->path);
    if (!matched_valid_request) {
        ctx->status = GF_INVALID;
        return -1;
    }
    if (strcmp(ctx->path, "") == 0) {
        ctx->status = GF_INVALID;
        return -1;
    }
    // The path must always start with a "/"
    if (strncmp(ctx->path, "/", 1) != 0) {
        ctx->status = GF_INVALID;
        return -1;
    }

    ctx->status = GF_OK;
    return 0;
}

int parse_request(gfcontext_t *ctx) {
    ssize_t num_bytes_read;
    ssize_t current_buffer_capacity = BUFSIZE;
    ssize_t current_buffer_size = 0;
    ssize_t current_buffer_free = BUFSIZE;
    char *buffer = calloc(current_buffer_free, sizeof(char));
    char *new_buffer;
    char *end_of_header_location;

    for (;;) {
        // Receive up to the buffer size bytes (minus 1 to leave space for a null terminator)
        num_bytes_read = read(ctx->client_socket_fd, buffer, current_buffer_free - 1);

        if (num_bytes_read <= 0) {
            free(buffer);
            ctx->status = GF_ERROR;
            return -1;
        }
        current_buffer_size += num_bytes_read;

        if (num_bytes_read < current_buffer_free - 1) {
            char temp_end_of_string = buffer[current_buffer_size];
            buffer[current_buffer_size] = '\0'; /* Terminate the string! */
            end_of_header_location = strstr(buffer, "\r\n\r\n");
            buffer[current_buffer_size] = temp_end_of_string;

            if(end_of_header_location) {
                *end_of_header_location = '\0'; /* Terminate the string! */
                break;
            } else {
                // Expected to see the end of header
                free(buffer);
                ctx->status = GF_INVALID;
                return -1;
            }
        }
        current_buffer_free -= num_bytes_read;
        if (current_buffer_free <= 2) {
            // memcopy old contents to new buffer
            new_buffer = calloc(current_buffer_capacity*2, sizeof(char));
            memcpy(new_buffer, buffer, current_buffer_capacity);
            current_buffer_capacity *= 2;
            // free old buffer memory
            free(buffer);
            buffer = new_buffer;
            current_buffer_free = current_buffer_capacity - current_buffer_size;
        }
    }

    ctx->path = calloc(current_buffer_capacity, sizeof(char));
    ssize_t parse_status = parse_path_from_header(ctx, buffer);

    free(buffer);
    return parse_status;
}

ssize_t gfs_sendheader(gfcontext_t *ctx, gfstatus_t status, size_t file_len) {
    char header[BUFSIZE];

    if (status == GF_OK) {
        snprintf(header, BUFSIZE, "GETFILE OK %zu\r\n\r\n", file_len);
    } else if (status == GF_FILE_NOT_FOUND) {
        snprintf(header, BUFSIZE, "GETFILE FILE_NOT_FOUND\r\n\r\n");
    } else if (status == GF_ERROR) {
        snprintf(header, BUFSIZE, "GETFILE ERROR\r\n\r\n");
    } else if (status == GF_INVALID) {
        snprintf(header, BUFSIZE, "GETFILE INVALID\r\n\r\n");
    }

    return gfs_send(ctx, header, strlen(header));
}

void cleanup_client_request(gfcontext_t *ctx) {
    if (!ctx) {
        return;
    }

    if (ctx->path) {
        free(ctx->path);
    }
    if (ctx->client_socket_fd > 0) {
        close(ctx->client_socket_fd);
    }
    free(ctx);
}

void gfserver_serve(gfserver_t *gfs) {
    int socket_fd = 0;
    int set_reuse_addr = 1; // ON == 1

    struct sockaddr_in server;
    struct sockaddr_in client;
    socklen_t client_addr_len;

    gfcontext_t *ctx;

    // Get the size client's address structure
    client_addr_len = sizeof(client);

    // Create socket (IPv4, stream-based, protocol likely set to TCP)
    if (0 > (socket_fd = socket(AF_INET, SOCK_STREAM, 0))) {
        /* fprintf(stderr, "server failed to create the listening socket\n"); */
        exit(1);
    }

    // Set socket to use wildcards - i.e. 0.0.0.0:21 and 192.168.0.1:21
    // can be bound separately (helps to avoid conflicts)
    if (0 != setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &set_reuse_addr, sizeof(set_reuse_addr))) {
        /* fprintf(stderr, "server failed to set SO_REUSEADDR socket option (not fatal)\n"); */
    }

    // Configure server socket address structure (init to zero, IPv4,
    // network byte order for port and address)
    // Address uses local wildcard 0.0.0.0.0 (will connect to any local addr)
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(gfs->port);

    // Bind the socket
    if (0 > bind(socket_fd, (struct sockaddr *)&server, sizeof(server))) {
        /* fprintf(stderr, "server failed to bind\n"); */
        exit(1);
    }

    // Listen on the socket for up to some maximum pending connections
    if (0 > listen(socket_fd, gfs->max_npending)) {
        /* fprintf(stderr, "server failed to listen\n"); */
        exit(1);
    }

    for (;;) {
        ctx = calloc(1, sizeof(gfcontext_t));

        // Accept a new client
        if (0 > (ctx->client_socket_fd = accept(socket_fd, (struct sockaddr *)&client, &client_addr_len))) {
            cleanup_client_request(ctx);
            continue;
        }
        if (parse_request(ctx) == 0) {
            gfs->handler(ctx, ctx->path, gfs->handlerarg);
        } else {
            gfs_sendheader(ctx, ctx->status, 0);
        }
        cleanup_client_request(ctx);
    }
}

void gfserver_set_handlerarg(gfserver_t *gfs, void* arg) {
    gfs->handlerarg = arg;
}

void gfserver_set_handler(gfserver_t *gfs, ssize_t (*handler)(gfcontext_t *, char *, void*)) {
    gfs->handler = handler;
}

void gfserver_set_maxpending(gfserver_t *gfs, int max_npending) {
    gfs->max_npending = max_npending;
}

void gfserver_set_port(gfserver_t *gfs, unsigned short port) {
    gfs->port = port;
}
