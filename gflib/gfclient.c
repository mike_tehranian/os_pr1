#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>

#include "gfclient.h"
#include "gfclient-student.h"

#define BUFSIZE 512

struct gfcrequest_t {
    char *server;
    char *path;
    unsigned short port;
    void (*headerfunc)(void*, size_t, void *);
    void *headerarg;
    void (*writefunc)(void*, size_t, void *);
    void *writearg;

    size_t bytesreceived;
    size_t filelen;
    gfstatus_t status;

    int socket_fd;
};

void gfc_cleanup(gfcrequest_t *gfr) {
    if (gfr->server) {
        free(gfr->server);
    }
    if (gfr->path) {
        free(gfr->path);
    }
    free(gfr);
}

gfcrequest_t *gfc_create() {
    gfcrequest_t *new_request = calloc(1, sizeof(gfcrequest_t));
    new_request->bytesreceived = 0;
    new_request->socket_fd = 0;

    return new_request;
}

size_t gfc_get_bytesreceived(gfcrequest_t *gfr) {
    return gfr->bytesreceived;
}

size_t gfc_get_filelen(gfcrequest_t *gfr) {
    return gfr->filelen;
}

gfstatus_t gfc_get_status(gfcrequest_t *gfr) {
    return gfr->status;
}

void gfc_global_init() {
}

void gfc_global_cleanup() {
}

int write_file_contents(gfcrequest_t *gfr, char *buffer, int header_length, ssize_t current_buffer_size) {
    if (!gfr->writefunc) {
        return -1;
    }
    char *file_contents = calloc(BUFSIZE, sizeof(char));
    int num_bytes_read;

    // Write out the contents already in the buffer past the header
    // current_buffer_size has an extra \0 hence the -1
    int bytes_read_past_header = current_buffer_size - header_length;
    if (bytes_read_past_header > 0) {
        gfr->bytesreceived += bytes_read_past_header;
        gfr->writefunc(buffer + header_length, bytes_read_past_header, gfr->writearg);
    }

    // Fetch and process the remaining bytes of the file contents
    while(gfr->bytesreceived < gfr->filelen) {
        num_bytes_read = read(gfr->socket_fd, file_contents, BUFSIZE);
        if (num_bytes_read < 0) {
            free(file_contents);
            /* gfr->status = GF_INVALID; */
            gfr->status = GF_ERROR;
            return 0;
        } else if (num_bytes_read == 0) {
            free(file_contents);
            /* gfr->status = GF_ERROR; */
            gfr->status = GF_OK;
            return -1;
        }
        gfr->bytesreceived += num_bytes_read;
        gfr->writefunc(file_contents, num_bytes_read, gfr->writearg);
    }

    free(file_contents);
    gfr->status = GF_OK;
    return 0;
}

int process_server_file_response(gfcrequest_t *gfr) {
    ssize_t num_bytes_read;
    ssize_t current_buffer_capacity = BUFSIZE;
    ssize_t current_buffer_size = 0;
    ssize_t current_buffer_free = current_buffer_capacity - current_buffer_size;
    char *buffer = calloc(current_buffer_free, sizeof(char));
    char *new_buffer;
    char *end_of_header_location;
    int found_header = 0;

    while (found_header == 0) {
        // Receive up to the buffer size bytes (minus 1 to leave space for a null terminator)
        num_bytes_read = read(gfr->socket_fd, buffer, current_buffer_free - 1);

        if (num_bytes_read < 0) {
            free(buffer);
            /* gfr->status = GF_INVALID; */
            gfr->status = GF_ERROR;
            return 0;
        } else if (num_bytes_read == 0) {
            free(buffer);
            /* gfr->status = GF_ERROR; */
            gfr->status = GF_INVALID;
            return -1;
        }
        current_buffer_size += num_bytes_read;

        char temp_end_of_string = buffer[current_buffer_size];
        buffer[current_buffer_size] = '\0'; /* Terminate the string! */
        end_of_header_location = strstr(buffer, "\r\n\r\n");
        buffer[current_buffer_size] = temp_end_of_string;

        if (end_of_header_location) {
            *end_of_header_location = '\0'; /* Terminate the string! */
            found_header = 1;
        }
        current_buffer_free -= num_bytes_read;
        if (current_buffer_free <= 2) {
            // memcopy old contents to new buffer
            new_buffer = calloc(current_buffer_capacity*2, sizeof(char));
            memcpy(new_buffer, buffer, current_buffer_capacity);
            current_buffer_capacity *= 2;
            // free old buffer memory
            free(buffer);
            buffer = new_buffer;
            current_buffer_free = current_buffer_capacity - current_buffer_size;
        }
    }

    int header_length = strlen(buffer) + strlen("\r\n\r\n");

    if (gfr->headerfunc) {
        gfr->headerfunc(buffer, header_length, gfr->headerarg);
    }

    int matched_ok_status = sscanf(buffer, "GETFILE OK %zu", &gfr->filelen);

    int return_status;
    if (matched_ok_status) {
        return_status = write_file_contents(gfr, buffer, header_length, current_buffer_size);
    } else if (strcmp(buffer, "GETFILE FILE_NOT_FOUND") == 0) {
        gfr->status = GF_FILE_NOT_FOUND;
        return_status = 0;
    } else if (strcmp(buffer, "GETFILE ERROR") == 0) {
        gfr->status = GF_ERROR;
        return_status = 0;
    } else {
        gfr->status = GF_INVALID;
        return_status = -1;
    }

    free(buffer);
    return return_status;
}

int send_request_to_server(int socket_fd, char* request) {
    size_t request_length = strlen(request) * sizeof(char);
    size_t num_bytes_remaining = request_length;
    size_t num_bytes_sent;
    size_t num_bytes_offset = 0;

    /* WARNNG: You cannot assume you can call free() on request */
    /* Doing a byte offset arithmetic in the way below is the only approach flexible enough */
    /* to work with both statically (char[]) and dynamically (char *) sized pointers */
    for (;;) {
        num_bytes_sent = write(socket_fd, request + num_bytes_offset, num_bytes_remaining);
        if (0 > num_bytes_sent) {
            return -1;
        } else if (num_bytes_sent >= num_bytes_remaining) {
            break;
        } else {
            num_bytes_remaining -= num_bytes_sent;
            num_bytes_offset += num_bytes_sent;
        }
    }

    if (num_bytes_sent != request_length) {
        return -1;
    }
    return 0;
}

int gfc_perform(gfcrequest_t *gfr) {
    struct sockaddr_in server_socket_addr;

    // Converts localhost into 0.0.0.0
    struct hostent *he = gethostbyname(gfr->server);
    unsigned long server_addr_nbo = *(unsigned long *)(he->h_addr_list[0]);

    // Create socket (IPv4, stream-based, protocol likely set to TCP)
    if ((gfr->socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        gfr->status = GF_ERROR;
        return -1;
    }

    // Configure server socket address structure (init to zero, IPv4,
    // network byte order for port and address)
    bzero(&server_socket_addr, sizeof(server_socket_addr));
    server_socket_addr.sin_family = AF_INET;
    server_socket_addr.sin_port = htons(gfr->port);
    server_socket_addr.sin_addr.s_addr = server_addr_nbo;

    // Connect socket to server
    if (connect(gfr->socket_fd, (struct sockaddr *)&server_socket_addr, sizeof(server_socket_addr)) < 0) {
        /* fprintf(stderr, "client failed to connect to %s:%d!\n", hostname, portno); */
        gfr->status = GF_ERROR;
        return -1;
    } else {
      /* fprintf(stdout, "client connected to to %s:%d!\n", hostname, portno); */
    }

    char get_file_request[BUFSIZE];
    snprintf(get_file_request, BUFSIZE, "GETFILE GET %s\r\n\r\n", gfr->path);
    if (send_request_to_server(gfr->socket_fd, get_file_request) < 0) {
        close(gfr->socket_fd);
        gfr->status = GF_ERROR;
        return -1;
    }

    int return_status = process_server_file_response(gfr);
    close(gfr->socket_fd);

    return return_status;
}

void gfc_set_headerarg(gfcrequest_t *gfr, void *headerarg) {
    gfr->headerarg = headerarg;
}

void gfc_set_headerfunc(gfcrequest_t *gfr, void (*headerfunc)(void*, size_t, void *)) {
    gfr->headerfunc = headerfunc;
}

void gfc_set_path(gfcrequest_t *gfr, char* path) {
    gfr->path = calloc(strlen(path) + 1, sizeof(char));
    strcpy(gfr->path, path);
}

void gfc_set_port(gfcrequest_t *gfr, unsigned short port) {
    gfr->port = port;
}

void gfc_set_server(gfcrequest_t *gfr, char* server) {
    gfr->server = calloc(strlen(server) + 1, sizeof(char));
    strcpy(gfr->server, server);
}

void gfc_set_writearg(gfcrequest_t *gfr, void *writearg) {
    gfr->writearg = writearg;
}

void gfc_set_writefunc(gfcrequest_t *gfr, void (*writefunc)(void*, size_t, void *)) {
    gfr->writefunc = writefunc;
}

char* gfc_strstatus(gfstatus_t status) {
    switch (status) {
        case GF_OK:
            return "OK";
        case GF_FILE_NOT_FOUND:
            // When the client has made an error in his request
            return "FILE_NOT_FOUND";
        case GF_ERROR:
            // When the server is responsible for something bad happening
            return "ERROR";
        default:
            // Invalid header
            // This includes a malformed header as well an incomplete header due
            // to communication issues.
            return "INVALID";
    }
}
