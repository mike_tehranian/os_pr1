#include <unistd.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define BUFSIZE 4096

#define USAGE                                                \
    "usage:\n"                                               \
    "  transferserver [options]\n"                           \
    "options:\n"                                             \
    "  -f                  Filename (Default: cs6200.txt)\n" \
    "  -h                  Show this help message\n"         \
    "  -p                  Port (Default: 6200)\n"

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
    {"filename", required_argument, NULL, 'f'},
    {"help", no_argument, NULL, 'h'},
    {"port", required_argument, NULL, 'p'},
    {NULL, 0, NULL, 0}};


FILE* open_serving_file(char *filename) {
    FILE *file_desc;
    file_desc = fopen(filename, "rb");

    if (file_desc == NULL) {
          fprintf(stderr, "Error opening filename: %s\n", filename);
          exit(1);
    }
    return file_desc;
}

int main(int argc, char **argv)
{
    int option_char;
    int portno = 6200;             /* port to listen on */
    char *filename = "cs6200.txt"; /* file to transfer */

    setbuf(stdout, NULL); // disable buffering

    // Parse and set command line arguments
    while ((option_char = getopt_long(argc, argv, "p:hf:x", gLongOptions, NULL)) != -1)
    {
        switch (option_char)
        {
        case 'p': // listen-port
            portno = atoi(optarg);
            break;
        default:
            fprintf(stderr, "%s", USAGE);
            exit(1);
        case 'h': // help
            fprintf(stdout, "%s", USAGE);
            exit(0);
            break;
        case 'f': // listen-port
            filename = optarg;
            break;
        }
    }


    if ((portno < 1025) || (portno > 65535))
    {
        fprintf(stderr, "%s @ %d: invalid port number (%d)\n", __FILE__, __LINE__, portno);
        exit(1);
    }

    if (NULL == filename)
    {
        fprintf(stderr, "%s @ %d: invalid filename\n", __FILE__, __LINE__);
        exit(1);
    }

    /* Socket Code Here */

    /* THE WARMUP CODE BELOW IS BASED UPON THE UDACITY PROBLEM SET 1 CODE */
    /* WHICH IS AVAILABLE ON THE COURSE LECTURE VIDEOS SITE: */
    /* https://classroom.udacity.com/courses/ud923/lessons/3518179061/concepts/34870090650923 */

    int socket_fd = 0;
    int client_socket_fd = 0;

    char buffer[BUFSIZE];

    int set_reuse_addr = 1; // ON == 1

    struct sockaddr_in server;
    struct sockaddr_in client;
    socklen_t client_addr_len;

    /* Note that your server should be capable of handling multiple connections even though no command line argument is required. */
    int maxnpending = 3;

    // Create socket (IPv4, stream-based, protocol likely set to TCP)
    if (0 > (socket_fd = socket(AF_INET, SOCK_STREAM, 0))) {
        /* fprintf(stderr, "server failed to create the listening socket\n"); */
        exit(1);
    }

    // Set socket to use wildcards - i.e. 0.0.0.0:21 and 192.168.0.1:21
    // can be bound separately (helps to avoid conflicts)
    if (0 != setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &set_reuse_addr, sizeof(set_reuse_addr))) {
        /* fprintf(stderr, "server failed to set SO_REUSEADDR socket option (not fatal)\n"); */
    }

    // Configure server socket address structure (init to zero, IPv4,
    // network byte order for port and address)
    // Address uses local wildcard 0.0.0.0.0 (will connect to any local addr)
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(portno);

    // Bind the socket
    if (0 > bind(socket_fd, (struct sockaddr *)&server, sizeof(server))) {
        /* fprintf(stderr, "server failed to bind\n"); */
        exit(1);
    }

    // Listen on the socket for up to some maximum pending connections
    if (0 > listen(socket_fd, maxnpending)) {
        /* fprintf(stderr, "server failed to listen\n"); */
        exit(1);
    } else {
        /* fprintf(stdout, "server listening for a connection on port %d\n", portno); */
    }

    // Get the size client's address structure
    client_addr_len = sizeof(client);

    // Create new file descriptor for the serving file
    FILE *file_reader;
    size_t num_bytes_read;

    for (;;) {
        // Accept a new client
        if (0 > (client_socket_fd = accept(socket_fd, (struct sockaddr *)&client, &client_addr_len))) {
            fprintf(stderr, "server accept failed\n");
            if (socket_fd > 0 && close(socket_fd) == -1) {
                printf("\nError closing socket\n");
                return 0;
            }
        } else {
            /* fprintf(stdout, "server accepted a client!\n"); */
        }

        // MDT learn how to set fd back to beginning
        file_reader = open_serving_file(filename);

        for (;;) {
            // Read 1 * BUFSIZE bytes from the file into buffer
            /* bzero(buffer, BUFSIZE); */
            num_bytes_read = fread(buffer, 1, BUFSIZE, file_reader);
            if (num_bytes_read > 0) {
                // Write contents to the client
                if (0 > write(client_socket_fd, buffer, num_bytes_read)) {
                    /* fprintf(stderr, "server could not write back to socket\n"); */
                    if (close(socket_fd) == -1) {
                      /* printf("\nError closing socket\n"); */
                      return 0;
                    }
                } else {
                    /* fprintf(stdout, "server sending message back to client\n"); */
                }
            }
            if (num_bytes_read < BUFSIZE) {
                break;
            }
        } // end file read while

        fclose(file_reader);
        close(client_socket_fd);
    } // end socket for

    // Close the socket and return
    if (close(socket_fd) == -1) {
        printf("\nError closing socket\n");
        return 0;
    }
}
