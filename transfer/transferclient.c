#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <getopt.h>

#define BUFSIZE 256

#define USAGE                                                \
    "usage:\n"                                               \
    "  transferclient [options]\n"                           \
    "options:\n"                                             \
    "  -s                  Server (Default: localhost)\n"    \
    "  -p                  Port (Default: 6200)\n"           \
    "  -o                  Output file (Default 6200.txt)\n" \
    "  -h                  Show this help message\n"

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
    {"server", required_argument, NULL, 's'},
    {"port", required_argument, NULL, 'p'},
    {"output", required_argument, NULL, 'o'},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}};

/* Main ========================================================= */
int main(int argc, char **argv)
{
    int option_char = 0;
    char *hostname = "localhost";
    unsigned short portno = 6200;
    char *filename = "6200.txt";

    setbuf(stdout, NULL);

    // Parse and set command line arguments
    while ((option_char = getopt_long(argc, argv, "s:p:o:hx", gLongOptions, NULL)) != -1)
    {
        switch (option_char)
        {
        case 's': // server
            hostname = optarg;
            break;
        case 'p': // listen-port
            portno = atoi(optarg);
            break;
        default:
            fprintf(stderr, "%s", USAGE);
            exit(1);
        case 'o': // filename
            filename = optarg;
            break;
        case 'h': // help
            fprintf(stdout, "%s", USAGE);
            exit(0);
            break;
        }
    }

    if (NULL == hostname)
    {
        fprintf(stderr, "%s @ %d: invalid host name\n", __FILE__, __LINE__);
        exit(1);
    }

    if (NULL == filename)
    {
        fprintf(stderr, "%s @ %d: invalid filename\n", __FILE__, __LINE__);
        exit(1);
    }

    if ((portno < 1025) || (portno > 65535))
    {
        fprintf(stderr, "%s @ %d: invalid port number (%d)\n", __FILE__, __LINE__, portno);
        exit(1);
    }

    /* Socket Code Here */

    /* THE WARMUP CODE BELOW IS BASED UPON THE UDACITY PROBLEM SET 1 CODE */
    /* WHICH IS AVAILABLE ON THE COURSE LECTURE VIDEOS SITE: */
    /* https://classroom.udacity.com/courses/ud923/lessons/3518179061/concepts/34870090650923 */

    int socket_fd = 0;
    struct sockaddr_in server_socket_addr;
    char buffer[BUFSIZE];

    // Converts localhost into 0.0.0.0
    struct hostent *he = gethostbyname(hostname);
    unsigned long server_addr_nbo = *(unsigned long *)(he->h_addr_list[0]);

    // Create socket (IPv4, stream-based, protocol likely set to TCP)
    if (0 > (socket_fd = socket(AF_INET, SOCK_STREAM, 0))) {
        /* fprintf(stderr, "client failed to create socket\n"); */
        exit(1);
    }

    // Configure server socket address structure (init to zero, IPv4,
    // network byte order for port and address)
    bzero(&server_socket_addr, sizeof(server_socket_addr));
    server_socket_addr.sin_family = AF_INET;
    server_socket_addr.sin_port = htons(portno);
    server_socket_addr.sin_addr.s_addr = server_addr_nbo;

    // Connect socket to server
    if (0 > connect(socket_fd, (struct sockaddr *)&server_socket_addr, sizeof(server_socket_addr))) {
        /* fprintf(stderr, "client failed to connect to %s:%d!\n", hostname, portno); */
        close(socket_fd);
        exit(1);
    } else {
      /* fprintf(stdout, "client connected to to %s:%d!\n", hostname, portno); */
    }

    FILE *write_file_desc = fopen(filename, "wb");
    if (write_file_desc == NULL) {
        fprintf(stderr, "Error opening write file: %s\n", filename);
    }

    // Process response from server
    bzero(buffer, BUFSIZE);
    size_t num_bytes_received;
    while((num_bytes_received = read(socket_fd, buffer, BUFSIZE)) > 0) {
        /* fprintf(stdout, "%s", buffer); */
        fwrite(buffer, 1, num_bytes_received, write_file_desc);
    }

    /* fprintf(stderr, "client could not read response from server\n"); */
    fclose(write_file_desc);

    // Close the socket and return the response length (in bytes)
    close(socket_fd);
    return 0;
}
