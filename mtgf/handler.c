#include <stdlib.h>
#include <fcntl.h>
#include <curl/curl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#include "gfserver.h"
#include "content.h"
#include "steque.h"

#include "gfserver-student.h"

#define BUFFER_SIZE 6200

// Thread safe queue global variables
pthread_mutex_t mutex_request_queue;
steque_t *request_queue;
pthread_cond_t cv_incoming_request;

// Params from a client request and server context
typedef struct {
    gfcontext_t *ctx;
    char *path;
    void *arg;
} request_queue_item;

//  The purpose of this function is to handle a get request
//
//  The ctx is the "context" operation and it contains connection state
//  The path is the path being retrieved
//  The arg allows the registration of context that is passed into this routine.
//  Note: you don't need to use arg. The test code uses it in some cases, but
//        not in others.
ssize_t getfile_handler(gfcontext_t *ctx, char *path, void* arg) {
    request_queue_item *new_request = calloc(1, sizeof(request_queue_item));
    new_request->ctx = ctx;
    new_request->path = path;
    new_request->arg = arg;

    pthread_mutex_lock(&mutex_request_queue);

    steque_enqueue(request_queue, new_request);

    // Notify (potentially) waiting threads that a new request has been enqueued
    // More than one thread is in a blocking state so broadcast
    pthread_cond_broadcast(&cv_incoming_request);

    // Failing to unlock the mutex after broadcasting will not allow
    // a matching pthread_cond_wait() routine to complete
    pthread_mutex_unlock(&mutex_request_queue);

    // No error back to the server main thread
    return 0;
}

void send_response(request_queue_item *new_request) {
    int file_desc = content_get(new_request->path);
    if (file_desc < 0) {
        gfs_sendheader(new_request->ctx, GF_FILE_NOT_FOUND, 0);
        return;
    }

    size_t file_len = lseek(file_desc, 0, SEEK_END);
    gfs_sendheader(new_request->ctx, GF_OK, file_len);

    char buffer[BUFFER_SIZE];
    size_t bytes_read, bytes_sent = 0, total_bytes_sent = 0;
    while (total_bytes_sent < file_len) {
        bytes_read = pread(file_desc, buffer, BUFFER_SIZE, total_bytes_sent);
        if (bytes_read <= 0) {
            gfs_abort(new_request->ctx);
            return;
        }
        bytes_sent = gfs_send(new_request->ctx, buffer, bytes_read);
        if (bytes_sent != bytes_read) {
            gfs_abort(new_request->ctx);
            return;
        }
        total_bytes_sent += bytes_sent;
    }
}

void* process_request(void *arg) {
    // Worker threads continuously loop and process each request in the queue
    for(;;) {
        pthread_mutex_lock(&mutex_request_queue);

        // Always use a while-loop with pthreads
        // See: https://computing.llnl.gov/tutorials/pthreads/#ConVarSignal
        while (steque_isempty(request_queue)) {
            pthread_cond_wait(&cv_incoming_request, &mutex_request_queue);
        }

        request_queue_item *new_request;
        new_request = steque_pop(request_queue);
        pthread_mutex_unlock(&mutex_request_queue);
        send_response(new_request);
    }

    pthread_exit(NULL);
}

// Initializes the three global variables
// Starts thread workers which wait for requests on the queue
void initialize_worker_pool(pthread_t *threads, int nthreads) {
    // Mutex for adding and removing requests
    pthread_mutex_init(&mutex_request_queue, NULL);

    // Conditional variable for workers waiting for new requests
    pthread_cond_init(&cv_incoming_request, NULL);

    // Thread-safe queue for requests
    request_queue = calloc(1, sizeof(steque_t));
    steque_init(request_queue);

    // Thread pool of workers
    for (int i = 0; i < nthreads; i++) {
        pthread_create(&threads[i], NULL, process_request, NULL);
    }
}

void cleanup_worker_pool() {
    steque_destroy(request_queue);
    free(request_queue);
}
