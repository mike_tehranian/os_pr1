#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <getopt.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>

#define BUFSIZE 256

#define USAGE                                                                 \
"usage:\n"                                                                    \
"  echoserver [options]\n"                                                    \
"options:\n"                                                                  \
"  -p                  Port (Default: 6200)\n"                                \
"  -m                  Maximum pending connections (default: 1)\n"            \
"  -h                  Show this help message\n"                              \

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
  {"port",          required_argument,      NULL,           'p'},
  {"maxnpending",   required_argument,      NULL,           'm'},
  {"help",          no_argument,            NULL,           'h'},
  {NULL,            0,                      NULL,             0}
};


int main(int argc, char **argv) {
  int option_char;
  int portno = 6200; /* port to listen on */
  int maxnpending = 1;

  // Parse and set command line arguments
  while ((option_char = getopt_long(argc, argv, "p:m:hx", gLongOptions, NULL)) != -1) {
   switch (option_char) {
      case 'p': // listen-port
        portno = atoi(optarg);
        break;
      default:
        fprintf(stderr, "%s ", USAGE);
        exit(1);
      case 'm': // server
        maxnpending = atoi(optarg);
        break;
      case 'h': // help
        fprintf(stdout, "%s ", USAGE);
        exit(0);
        break;
    }
  }

    setbuf(stdout, NULL); // disable buffering

    if ((portno < 1025) || (portno > 65535)) {
        fprintf(stderr, "%s @ %d: invalid port number (%d)\n", __FILE__, __LINE__, portno);
        exit(1);
    }
    if (maxnpending < 1) {
        fprintf(stderr, "%s @ %d: invalid pending count (%d)\n", __FILE__, __LINE__, maxnpending);
        exit(1);
    }


  /* Socket Code Here */

  /* THE WARMUP CODE BELOW IS BASED UPON THE UDACITY PROBLEM SET 1 CODE */
  /* WHICH IS AVAILABLE ON THE COURSE LECTURE VIDEOS SITE: */
  /* https://classroom.udacity.com/courses/ud923/lessons/3518179061/concepts/34870090650923 */

  int socket_fd = 0;
  int client_socket_fd = 0;

  char buffer[BUFSIZE];
  int num_bytes = 0;

  int set_reuse_addr = 1; // ON == 1

  struct sockaddr_in server;
  struct sockaddr_in client;
  struct hostent *client_host_info;
  /* char *client_host_ip; */
  socklen_t client_addr_len;

  // Create socket (IPv4, stream-based, protocol likely set to TCP)
  if (0 > (socket_fd = socket(AF_INET, SOCK_STREAM, 0))) {
    /* fprintf(stderr, "server failed to create the listening socket\n"); */
    exit(1);
  }

  // Set socket to use wildcards - i.e. 0.0.0.0:21 and 192.168.0.1:21
  // can be bound separately (helps to avoid conflicts)
  if (0 != setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &set_reuse_addr, sizeof(set_reuse_addr))) {
    /* fprintf(stderr, "server failed to set SO_REUSEADDR socket option (not fatal)\n"); */
  }

  // Configure server socket address structure (init to zero, IPv4,
  // network byte order for port and address)
  // Address uses local wildcard 0.0.0.0.0 (will connect to any local addr)
  bzero(&server, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(portno);

  // Bind the socket
  if (0 > bind(socket_fd, (struct sockaddr *)&server, sizeof(server))) {
    /* fprintf(stderr, "server failed to bind\n"); */
    exit(1);
  }

  // Listen on the socket for up to some maximum pending connections
  if (0 > listen(socket_fd, maxnpending)) {
    /* fprintf(stderr, "server failed to listen\n"); */
    exit(1);
  } else {
    /* fprintf(stdout, "server listening for a connection on port %d\n", portno); */
  }

  // Get the size client's address structure
  client_addr_len = sizeof(client);

  for (;;) {
    // Accept a new client
    if (0 > (client_socket_fd = accept(socket_fd, (struct sockaddr *)&client, &client_addr_len))) {
      /* fprintf(stderr, "server accept failed\n"); */
      if (socket_fd > 0 && close(socket_fd) == -1) {
        /* printf("\nError closing socket\n"); */
        /* return 0; */
      }
      return 0;
    } else {
      /* fprintf(stdout, "server accepted a client!\n"); */
    }

    // Determine who sent the echo so that we can respond
    client_host_info = gethostbyaddr((const char *)&client.sin_addr.s_addr, sizeof(client.sin_addr.s_addr), AF_INET);
    if (client_host_info == NULL) {
      /* fprintf(stderr, "server could not determine client host address\n"); */
    }
    /* client_host_ip = inet_ntoa(client.sin_addr); */
    /* if (client_host_ip == NULL) { */
    /*   fprintf(stderr, "server could not determine client host ip\n"); */
    /* } */
    /* fprintf(stdout, "server established connection with %s (%s)\n", client_host_info->h_name, client_host_ip); */

    // Read echo from the client
    bzero(buffer, BUFSIZE);
    num_bytes = read(client_socket_fd, buffer, BUFSIZE);
    if (num_bytes == 0) {
      /* fprintf(stderr, "server could not read from socket\n"); */
      if (close(socket_fd) == -1) {
        /* printf("\nError closing socket\n"); */
        /* return 0; */
      }
      return 0;
    } else {
      /* fprintf(stdout, "server received %d bytes: %s\n", num_bytes, buffer); */
    }

    // Echo back to the client
    if (0 > write(client_socket_fd, buffer, strlen(buffer))) {
      /* fprintf(stderr, "server could not write back to socket\n"); */
      if (close(socket_fd) == -1) {
        /* printf("\nError closing socket\n"); */
        /* return 0; */
      }
      return 0;
    } else {
      /* fprintf(stdout, "server sending message back to client\n"); */
    }
  }

  // Close the socket and return
  if (close(socket_fd) == -1) {
    /* printf("\nError closing socket\n"); */
    return 0;
  }
}
